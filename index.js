let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.
*/

// Solution
function addUsers (user) {
    
    users[users.length] = user;
    console.log(users);

} addUsers('John Cena');

/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

// Solution
let itemFound;
function printAccessedArray(user) {

    itemFound = user;
    console.log(itemFound);
    return printAccessedArray;

} printAccessedArray(users[2]);


/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/
// Solution
function printLastIndex(){

    // Accessing the Last Index of ArrayElement
    let lastIndex = users.length -1;
    console.log(users[lastIndex]);

    // Deleting the Last Element of the Last Index of ArrayElement
    users.length = users.length - 1;
    console.log(users);


    return printLastIndex;
}
printLastIndex();



/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.
*/
// Solution

function updateIndex(user,update){

    users[3] = update;
    console.log(users);

} updateIndex(users[3], "Triple H");




/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/
// Solution
function deleteAllIndex(){

    users.length = users.length - 4; // myTask = 4 -1 = 3
    console.log(users);

}
deleteAllIndex();


/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/
// Solution
let isUSersEmpty = 0;
function checkEmptyArray () {

    if(isUSersEmpty > users.length){

    } else{

        console.log(isUSersEmpty === 0);
    } 

}
checkEmptyArray();


/*let newVar = "";
for(let i = 0; i<6; i++) {
    newVar += " ".repeat(i) + "0".repeat(5-i)+"\n";

}
console.log(newVar);
alert(newVar);
*/